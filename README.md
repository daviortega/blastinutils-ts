# blastinutils-ts

![npm](https://img.shields.io/npm/v/blastinutils-ts.svg)
![License: CC0-1.0](https://img.shields.io/badge/License-CC0%201.0-blue.svg)
[![pipeline status](https://gitlab.com/daviortega/blastinutils-ts/badges/master/pipeline.svg)](https://gitlab.com/daviortega/blastinutils-ts/commits/master)
[![coverage report](https://gitlab.com/daviortega/blastinutils-ts/badges/master/coverage.svg)](https://mist3.gitlab.io/blastinutils-ts/coverage)

Super simple helper library to BLAST software from NCBI

## Install

```bash
npm install blastinutils-ts
```

## Support

`blastp`
`makeblastdb`

## Usage

### CommandToolKit

This is super simple, just build the parameters following the interfaces and ask to build the command.

```javascript
import { CommandToolKit, interfaces } from 'blastinutils-ts'

const params: IBlastPParameters = {
  db: 'mydb',
  evalue: 1,
  num_threads: 4,
  out: 'myoutputFile.dat',
  outfmt: {
    format: 6,
    parameters: [
      'qseqid',
      'sseqid',
      'bitscore',
      'pident',
      'evalue',
      'length',
    ]
  },
  query: 'myquery'
}

const program: interfaces.supportedProgramsType = 'blastp'
const command = commandTk.build(program, params)
console.log(command)
// blastp -db mydb -evalue 1 -num_threads 4 -out myoutputFile.dat -outfmt "6 qseqid sseqid bitscore pident evalue length" -query myquery

```

### NodesAndLinksStream

This is an experimental parser of BLAST results in output format `6`. Because these tend to be long, we implemented as a write stream. After reading the stream, the class has a method: `getData()` which returns an object `{ nodes: [ ... ], links: [ ... ] }` with the data.

```javascript
import { NodesAndLinksStream } from 'blastinutils-ts'

const params = {
  format: 6,
  parameters: [
    'qseqid',
    'sseqid',
    'bitscore',
    'pident',
    'evalue',
    'length',
  ]
}

const nodesNlinks = new NodesAndLinksStream.ParseBlastResults(params)
  const blastResultsStream = fs.createReadStream('tabularBlastResults.fmt6.tab')
  blastResultsStream
    .pipe(nodesNlinks)
    .on('finish',() => {
      const data = nodesNlinks.getData()
      /* data.nodes = [
        'seq1',
        'seq2',
        ...
      ]
      // data.links = [
        {
          s: 0, // index of source in data.nodes
          t: 1, // index of target in data.nodes
          e: 20
        },
        {
          s: 0,
          t: 2,
          e: 10
        }
      ]

      */
    })
```

## Documentation

[Developer's documentation](https://daviortega.gitlab.io/blastinutils-ts/)

...to be continued.

Written with ❤ in Typescript.
