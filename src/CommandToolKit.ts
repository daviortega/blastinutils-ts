import { Logger, LogLevelDescType } from "loglevel-colored-prefix";
import {
  IBlastPParameters,
  IMakeBlastDBParameters,
  supportedProgramsType
} from "./interfaces";

/**
 * Helper class to build BLAST commands with parameters
 *
 * @class CommandToolKit
 */
class CommandToolKit {
  private readonly logger: Logger;

  public constructor(logLevel: LogLevelDescType = "INFO") {
    this.logger = new Logger(logLevel);
  }

  /**
   * Builds the command.
   *
   * @param {supportedProgramsType} program
   * @param {(IBlastPParameters | IMakeBlastDBParameters)} [params={}]
   * @returns
   * @memberof CommandToolKit
   */
  public build(
    program: supportedProgramsType,
    params: IBlastPParameters | IMakeBlastDBParameters = {}
  ) {
    const log = this.logger.getLogger("CommandsToolKit::buildCommand");
    const command = this.parseParams(params, program);
    log.debug(`Command built: ${command}`);
    return command;
  }

  /**
   * Parse the parameters.
   *
   * @private
   * @param {(IMakeBlastDBParameters | IBlastPParameters)} params
   * @param {supportedProgramsType} program
   * @returns
   * @memberof CommandToolKit
   */
  private parseParams(
    params: IMakeBlastDBParameters | IBlastPParameters,
    program: supportedProgramsType
  ) {
    const commandParams: string[] = [program];
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        if (key === "outfmt") {
          const outfmt = params.outfmt;
          if (outfmt) {
            commandParams.push(`-outfmt`);
            const outFmtParam = outfmt.parameters.join(" ");
            commandParams.push(`"${outfmt.format} ${outFmtParam}"`);
          }
        } else {
          switch (program) {
            case "blastp": {
              const typedParams = params as IBlastPParameters;
              const typedKey = key as keyof IBlastPParameters;
              const value = typedParams[typedKey];
              commandParams.push(this.parseOneParameter(typedKey, value));
              break;
            }
            case "makeblastdb": {
              const typedParams = params as IMakeBlastDBParameters;
              const typedKey = key as keyof IMakeBlastDBParameters;
              const value = typedParams[typedKey];
              commandParams.push(this.parseOneParameter(typedKey, value));
              break;
            }
            default:
              throw new Error(`The ${program} isn't supported by the parser.`);
          }
        }
      }
    }
    return commandParams.join(" ");
  }

  /**
   * Parse flag parameters differently from others.
   *
   * @private
   * @param {string} key
   * @param {*} value
   * @returns
   * @memberof CommandToolKit
   */
  private parseOneParameter(key: string, value: any) {
    if (typeof value === "boolean") {
      return `-${key}`;
    }
    return `-${key} ${value}`;
  }
}

export { CommandToolKit };
