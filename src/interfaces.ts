type outputParameterType =
  | "qseqid" // means Query Seq-id
  | "qgi" // means Query GI
  | "qacc" // means Query accesion
  | "sseqid" // means Subject Seq-id
  | "sallseqid" // means All subject Seq-id(s), separated by a ';'
  | "sgi" // means Subject GI
  | "sallgi" // means All subject GIs
  | "sacc" // means Subject accession
  | "sallacc" // means All subject accessions
  | "qstart" // means Start of alignment in query
  | "qend" // means End of alignment in query
  | "sstart" // means Start of alignment in subject
  | "send" // means End of alignment in subject
  | "qseq" // means Aligned part of query sequence
  | "sseq" // means Aligned part of subject sequence
  | "evalue" // means Expect value
  | "bitscore" // means Bit score
  | "score" // means Raw score
  | "length" // means Alignment length
  | "pident" // means Percentage of identical matches
  | "nident" // means Number of identical matches
  | "mismatch" // means Number of mismatches
  | "positive" // means Number of positive-scoring matches
  | "gapopen" // means Number of gap openings
  | "gaps" // means Total number of gap
  | "ppos" // means Percentage of positive-scoring matches
  | "frames" // means Query and subject frames separated by a '/'
  | "qframe" // means Query frame
  | "sframe" // means Subject frame
  | "btop" // means Blast traceback operations (BTOP)
  | "staxids" // means unique Subject Taxonomy ID(s), separated by a ';'(in numerical order)
  | "sscinames" // means unique Subject Scientific Name(s), separated by a ';'
  | "scomnames" // means unique Subject Common Name(s), separated by a ';'
  | "sblastnames" // means unique Subject Blast Name(s), separated by a ';' (in alphabetical order)
  | "sskingdoms" // means unique Subject Super Kingdom(s), separated by a ';' (in alphabetical order)
  | "stitle" // means Subject Title
  | "salltitles" // means All Subject Title(s), separated by a '<>'
  | "sstrand" // means Subject Strand
  | "qcovs" // means Query Coverage Per Subject (for all HSPs)
  | "qcovhsp" // means Query Coverage Per HSP
  | "qcovus"; // is a measure of Query Coverage that counts a position in a subject sequence for this measure only once. The second time the position is aligned to the query is not counted towards this measure.

interface IOutputFormat {
  format:
    | 0
    | 1
    | 2
    | 3
    | 4
    | 5
    | 6
    | 7
    | 8
    | 9
    | 10
    | 11
    | 12
    | 13
    | 14
    | 15
    | 16
    | 17
    | 18;
  /* Default: 0. Description: alignment view options:
  0 = pairwise,
  1 = query-anchored showing identities,
  2 = query-anchored no identities,
  3 = flat query-anchored, show identities,
  4 = flat query-anchored, no identities,
  5 = XML Blast output,
  6 = tabular,
  7 = tabular with comment lines,
  8 = Text ASN.1,
  9 = Binary ASN.1
  10 = Comma-separated values
  11 = BLAST archive format (ASN.1)
  12 = Seqalign (JSON),
  13 = Multiple-file BLAST JSON,
  14 = Multiple-file BLAST XML2,
  15 = Single-file BLAST JSON,
  16 = Single-file BLAST XML2,
  17 = Sequence Alignment/Map (SAM),
  18 = Organism Report
  Options 6, 7, and 10 can be additionally configured to produce a custom format specified by space delimited format specifiers. */
  parameters: outputParameterType[];
}

interface IBlastGenericParameters {
  db?: string; // Default: none. Description: BLAST database name.
  query?: string; // Default: stdin. Description: Query file name.
  query_loc?: string; // Default: none. Description: Location on the query sequence (Format: start-stop)
  out?: string; // Default: stdout. Description: Output file name
  evalue?: number; // Default: 10. Description:.0 Expect value (E) for saving hits
  subject?: string; // Default: none. Description: File with subject sequence(s) to search.
  subject_loc?: string; // Default: none. Description: Location on the subject sequence (Format: start-stop).
  show_gis?: boolean; // Default: N. Description:/A Show NCBI GIs in report.
  num_descriptions?: number; // Default: 500. Description: Show one-line descriptions for this number of database sequences.
  num_alignments?: number; // Default: 250. Description: Show alignments for this number of database sequences.
  max_target_seqs?: number; // Default: 500. Description: Number of aligned sequences to keep. Use with report formats that do not have separate definition line and alignment sections such as tabular (all outfmt > 4). Not compatible with num_descriptions or num_alignments. Ties are broken by order of sequences in the database.
  max_hsps?: number; // Default: none. Description: Maximum number of HSPs (alignments) to keep for any single query-subject pair. The HSPs shown will be the best as judged by expect value. This number should be an integer that is one or greater. If this option is not set, BLAST shows all HSPs meeting the expect value criteria. Setting it to one will show only the best HSP for every query-subject pair
  html?: boolean; // Default: N. Description:/A Produce HTML output
  gilist?: string; // Default: none. Description: Restrict search of database to GI’s listed in this file. Local searches only.
  negative_gilist?: string; // Default: none. Description: Restrict search of database to everything except the GI’s listed in this file. Local searches only.
  entrez_query?: string; // Default: none. Description: Restrict search with the given Entrez query. Remote searches only.
  culling_limit?: number; // Default: none. Description: Delete a hit that is enveloped by at least this many higher-scoring hits.
  best_hit_overhang?: number; // Default: none. Description: Best Hit algorithm overhang value (recommended value: 0.1)
  best_hit_score_edge?: number; // Default: none. Description: Best Hit algorithm score edge value (recommended value: 0.1)
  dbsize?: number; // Default: none. Description: Effective size of the database
  searchsp?: number; // Default: none. Description: Effective length of the search space
  import_search_strategy?: string; // Default: none. Description: Search strategy file to read.
  export_search_strategy?: string; // Default: none. Description: Record search strategy to this file.
  parse_deflines?: boolean; // Default: N. Description:/A Parse query and subject bar delimited sequence identifiers (e.g., gi|129295).
  num_threads?: number; // Default: 1. Description: Number of threads (CPUs) to use in blast search.
  remote?: boolean; // Default: N. Description:/A Execute search on NCBI servers?
  outfmt?: IOutputFormat;
}

interface IBlastPParameters extends IBlastGenericParameters {
  word_size?: number; // Default: 3. Description: Word size of initial match. Valid word sizes are 2-7.
  gapopen?: number; // Default: 11. Description: Cost to open a gap.
  gapextend?: number; // Default: 1. Description: Cost to extend a gap.
  matrix?: string; // Default: BLOSUM62. Description: Scoring matrix name.
  threshold?: number; // Default: 11. Description: Minimum score to add a word to the BLAST lookup table.
  comp_based_stats?: string /* Default: 2. Description:
    D or d: default (equivalent to 2)
    0 or F or f: no composition-based statistics
    1: Composition-based statistics as in NAR 29:2994-3005, 2001
    2 or T or t : Composition-based score adjustment as in Bioinformatics
    21:902-911, 2005, conditioned on sequence properties
    3: Composition-based score adjustment as in Bioinformatics 21:902-911, 2005, unconditionally
  */;
  seg?: string; // Default: no. Description: Filter query sequence with SEG (Format: 'yes', 'window locut hicut', or 'no' to disable).
  soft_masking?: boolean; // Default: false. Description: Apply filtering locations as soft masks (i.e., only for finding initial matches).
  lcase_masking?: boolean; // Default: N/A. Description: Use lower case filtering in query and subject sequence(s).
  db_soft_mask?: number; // Default: none. Description: Filtering algorithm ID to apply to the BLAST database as soft mask (i.e., only for finding initial matches).
  db_hard_mask?: number; // Default: none. Description: Filtering algorithm ID to apply to the BLAST database as hard mask (i.e., sequence is masked for all phases of search).
  xdrop_gap_final?: number; // Default: 25. Description: Heuristic value (in bits) for final gapped alignment/
  window_size?: number; // Default: 40. Description: Multiple hits window size, use 0 to specify 1-hit algorithm.
  use_sw_tback?: boolean; // Default: N/A. Description: Compute locally optimal Smith-Waterman alignments?
}

type makeBlastDBInputFiles = "fasta" | "blastdb" | "asn1_txt" | "asn1_bin";
/*
  fasta?:  for, // Default: FASTA. Description: file(s)
  blastdb?:  for, // Default: BLAST. Description: database(s)
  asn1_txt?:  for, // Default: Seq. Description:-entries in text ASN.1 format
  asn1_bin?:  for, // Default: Seq. Description:-entries in binary ASN.1 format
*/

type makeDBtype = "nucl" | "prot";

type blastdbVersionType = 4 | 5;

interface IMakeBlastDBParameters extends IBlastGenericParameters {
  in?: string; // Default: stdin. Description: Input file/database name
  input_type?: makeBlastDBInputFiles; // Default: fasta. Description: Input file type, it may be any of the following:
  dbtype?: makeDBtype; // Default: prot. Description: Molecule type of input, values can be nucl or prot.
  title?: string; // Default: none. Description: Title for BLAST database. If not set, the input file name will be used.
  parse_seqids?: boolean; // Default: False. Description: Parse bar delimited sequence identifiers (e.g., gi|129295) in FASTA input.
  blastdb_version?: blastdbVersionType; // Default: 5. Description: 5 (starting BLAST+ 2.10.0). Valid values are 4 and 5.
  hash_index?: boolean; // Default: False. Description: Create index of sequence hash values.
  mask_data?: string; // Default: none. Description: Comma-separated list of input files containing masking data as produced by NCBI masking applications (e.g. dustmasker, segmasker, windowmasker).
  out?: string; // Default: input. Description: file name Name of BLAST database to be created. Input file name is used if none provided. This field is required if input consists of multiple files.
  max_file_size?: string; // Default: 1GB. Description: Maximum file size to use for BLAST database.
  taxid?: number; // Default: none. Description: Taxonomy ID to assign to all sequences.
  taxid_map?: string; // Default: none.
  /*
    Description: File with two columns mapping sequence ID to the taxonomy ID. The first column is the sequence ID represented as one of:
    1. fasta with accessions (e.g., emb|X17276.1|)
    2. fasta with GI (e.g., gi|4)
    3. GI as a bare number (e.g., 4)
    4. A local ID. The local ID must be prefixed with "lcl" (e.g., lcl|4).
    The second column should be the NCBI taxonomy ID (e.g., 9606 for human).
  */
  logfile?: string; // Default: none. Description: Program log file (default is stderr).
}

type supportedProgramsType = "blastp" | "makeblastdb";

interface ILink {
  s: number;
  t: number;
  e: number;
}

export {
  IBlastPParameters,
  supportedProgramsType,
  IMakeBlastDBParameters,
  IBlastGenericParameters,
  IOutputFormat,
  ILink
};
