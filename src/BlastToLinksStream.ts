import { Logger, LogLevelDescType } from "loglevel-colored-prefix";
import stream from "stream";
import { ILink, IOutputFormat } from "./interfaces";

/**
 * Extension of IOutputFormat to make sure user knows that only format 6 is supported
 *
 * @interface ISupportedParams
 * @extends {IOutputFormat}
 */
interface ISupportedParams extends IOutputFormat {
  format: 6;
}

/**
 * Internal interface to build parsed object with line data
 *
 * @interface IParsedLine
 */
interface IParsedLine {
  [key: string]: string | number;
}

interface IRawLinks {
  [key: string]: number;
}

const Writable = stream.Writable;

const kDefaults = {
  maxLogEvalue: 200,
  supportedFormats: [6]
};

/**
 * Stream to parse BLASTP results in tabular format (6) into links between nodes with -log10(Evalue) as link value. d3js style.
 *
 * @class WritableParser
 * @extends {Writable}
 */
class WritableParser extends Writable {
  private readonly logger: Logger;
  private readonly outfmtParams: ISupportedParams;
  private dataIndex: string[];
  private rawLinks: IRawLinks;
  private links: ILink[];
  private buffer: string;

  public constructor(
    dataIndex: string[],
    outfmtParams: ISupportedParams,
    logLevel: LogLevelDescType = "INFO"
  ) {
    super({ objectMode: true });
    this.logger = new Logger(logLevel);
    this.outfmtParams = outfmtParams;
    if (this.outfmtParams.format !== 6) {
      throw new Error(
        `Format not supported: ${
          this.outfmtParams.format
        }. Supported formats: ${kDefaults.supportedFormats.join(", ")}.`
      );
    }
    this.buffer = "";
    this.dataIndex = dataIndex;
    this.links = [];
    this.rawLinks = {};
  }

  /**
   * Return the data of nodes and links after parsing the BLAST results.
   *
   * @returns {{ nodes: nodeType[], links: ILink[] }}
   * @memberof WritableParser
   */
  public getData(): ILink[] {
    return this.links;
  }

  /**
   * Implementation of stream logic as it writes .nodes and .links
   *
   * @param {(Buffer | undefined)} chunk
   * @param {*} enc
   * @param {() => void} next
   * @memberof WritableParser
   */
  public _write(chunk: Buffer | undefined, enc: any, next: () => void) {
    this.processNewChunk(chunk);
    next();
  }

  /**
   * Final flush of BLAST tabular data
   *
   * @param {() => void} next
   * @memberof WritableParser
   */
  public _final(next: () => void) {
    const log = this.logger.getLogger("WritableParser::final");
    this.processNewChunk();
    this.parseLinks();
    log.debug(this.dataIndex);
    log.debug(this.links);
    next();
  }

  /**
   * Parser of 1 complete line of tabular data in format 6
   *
   * @protected
   * @param {string} line
   * @returns
   * @memberof WritableParser
   */
  protected parseTabularData(line: string) {
    const log = this.logger.getLogger("WritableParser::processNewChunk");
    const params = this.outfmtParams.parameters;
    const valueList = line.replace(/\n/, "").split("\t");
    const object: IParsedLine = {};
    log.debug(valueList);
    log.debug(params.length);
    if (valueList.length === params.length) {
      params.forEach((key, i) => {
        const value = valueList[i];
        if (isNaN(Number(value))) {
          object[key] = valueList[i];
        } else {
          object[key] = Number(value);
        }
      });
      return JSON.parse(JSON.stringify(object));
    }
    return line;
  }

  /**
   * Process chunk of tabular data
   *
   * @private
   * @param {Buffer} [chunk]
   * @memberof WritableParser
   */
  private processNewChunk(chunk?: Buffer) {
    const log = this.logger.getLogger("WritableParser::processNewChunk");
    if (chunk) {
      this.buffer += chunk.toString();
    }
    const lines: string[] = this.buffer.split("\n");
    if (chunk) {
      this.buffer = lines.pop() || "";
    }
    for (const line of lines) {
      const parsedLine = this.parseTabularData(line);
      if (typeof parsedLine !== "string") {
        try {
          this.addLink(parsedLine);
        } catch (error) {
          this.emit("error", error);
        }
        log.debug(`Links: ${this.links.length}`);
      } else if (line === "") {
        log.info("end of file");
      } else {
        log.error(line);
        this.emit("error", new Error("BLAST data seems to be corrupt."));
      }
    }
  }

  /**
   * Add a new link to .links
   *
   * @private
   * @param {IParsedLine} lineObject
   * @memberof WritableParser
   */
  private addLink(lineObject: IParsedLine) {
    const s = this.dataIndex.indexOf(lineObject.qseqid as string);
    const t = this.dataIndex.indexOf(lineObject.sseqid as string);
    if (s === -1) {
      throw new Error(`Query not present in index: ${lineObject.qseqid}`);
    }
    if (t === -1) {
      throw new Error(`Hit not present in index: ${lineObject.sseqid}`);
    }

    const linkKey = `${s}.${t}`;
    const e =
      lineObject.evalue === 0.0
        ? kDefaults.maxLogEvalue
        : -Math.log10(lineObject.evalue as number).toFixed(2);
    if (!(this.rawLinks[linkKey] > e)) {
      this.rawLinks[linkKey] = e;
    }
  }

  private parseLinks() {
    for (const key of Object.keys(this.rawLinks)) {
      const st = key.split(".");
      this.links.push({
        e: this.rawLinks[key],
        s: parseInt(st[0], 10),
        t: parseInt(st[1], 10)
      });
    }
  }
}

export { WritableParser, ISupportedParams };
