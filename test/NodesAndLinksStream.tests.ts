import chai from 'chai'
import fs from 'fs'
import { ISupportedParams, WritableParser } from '../src/BlastToLinksStream'

const expect = chai.expect
const logLevel = 'silent'

describe.only('WritableParser', () => {
  const params: ISupportedParams = {
    format: 6,
    parameters: [
      'qseqid',
      'sseqid',
      'bitscore',
      'pident',
      'evalue',
      'length',
    ]
  }
  describe('parseTabularData', () => {
    class MockClass extends WritableParser {
      public parseTabularData(line: string) {
        return super.parseTabularData(line)
      }
    }
    it('should parse complete line', () => {
      const line = 'Ps_aer|GCF_000006765.1-PA0973	Al_ill|GCF_000619845.1-Q337_RS0100765	130	51.471	9.05e-42	136'
      const index = [
        'Ps_aer|GCF_000006765.1-PA0973',
        'Al_ill|GCF_000619845.1-Q337_RS0100765'
      ]
      const parser = new MockClass(index, params, logLevel)
      const parsedLine = parser.parseTabularData(line)
      const expectedParsedLine = {
        bitscore: 130,
        evalue: 9.05e-42,
        length: 136,
        pident: 51.471,
        qseqid: 'Ps_aer|GCF_000006765.1-PA0973',
        sseqid: 'Al_ill|GCF_000619845.1-Q337_RS0100765'
      }
      expect(parsedLine).eql(expectedParsedLine)
    })
    it('should return incomplete line', () => {
      const line = 'Ps_aer|GCF_000006765.1-PA0973	Al_ill|GCF_000619845.1-Q337_RS0100765	130	51.471	9.05e-42'
      const index = [
        'Ps_aer|GCF_000006765.1-PA0973',
        'Al_ill|GCF_000619845.1-Q337_RS0100765'
      ]
      const parser = new MockClass(index, params, logLevel)
      const parsedLine = parser.parseTabularData(line)
      const expectedParsedLine = line
      expect(parsedLine).eql(expectedParsedLine)
    })
  })
  describe('stream', () => {
    it('should parse the data', () => {
			const index = [
				'Ps_aer|GCF_000006765.1-PA0685',
				'Ps_aer|GCF_000006765.1-PA1868',
				'Ps_aer|GCF_000006765.1-PA3105',
				'Mu_mur|GCF_001038205.1-RO21_RS00750',
				'Ps_aer|GCF_000006765.1-PA1382',
				'Ps_aer|GCF_000006765.1-PA1716',
				'Ps_aer|GCF_000006765.1-PA2633'
			]
			const expectedLinks = [
				{
          e: 200,
          s: 0,
					t: 0					
				},
				{
          e: 84.44,
          s: 0,
					t: 1					
				},
				{
          e: 51.09,
          s: 0,
					t: 2					
				},
				{
          e: 25.01,
          s: 0,
					t: 3					
				},
				{
          e: 25.04,
          s: 4,
					t: 0					
				},
				{
          e: 200,
          s: 5,
					t: 5					
				},
				{
          e: 0.29,
          s: 1,
					t: 6					
				},
				{
          e: 2.3,
          s: 2,
					t: 4					
				},
			]
			const parser = new WritableParser(index, params, logLevel)
      const blastResultsStream = fs.createReadStream('test-data/tabularBlastResults.fmt6.tab')
      blastResultsStream
				.pipe(parser)
				.on('finish',() => {
          const links = parser.getData()
					expect(links).eql(expectedLinks)
        })
    })
    it('should throw if query not found in index', () => {
			const index = [
        'Ps_aer|GCF_000006765.1-PA0685',
				'Ps_aer|GCF_000006765.1-PA3105',
				'Mu_mur|GCF_001038205.1-RO21_RS00750',
				'Ps_aer|GCF_000006765.1-PA1382',
				'Ps_aer|GCF_000006765.1-PA1716',
				'Ps_aer|GCF_000006765.1-PA2633'
			]
			const parser = new WritableParser(index, params, logLevel)
      const blastResultsStream = fs.createReadStream('test-data/tabularBlastResults.fmt6.tab')
      blastResultsStream
				.pipe(parser)
				.on('finish',() => {
          const links = parser.getData()
					// tslint:disable-next-line: no-unused-expression
					expect(links,'The stream should have emitted error and not output any result.').to.be.undefined
        })
        .on('error',(error) => {
          expect(error.message).to.match(/Ps_aer\|GCF_000006765\.1-PA1868$/)
        })
    })
  })
  describe('efficiency', () => {
    it('should not take too long with large datasets', function(done) {
      this.timeout(15000)
        const index = fs.readFileSync('test-data/tabularBlastResults.fmt6.large.dataIndex.lst').toString().split('\n').filter(d => d !== '')
        const parser = new WritableParser(index, params, logLevel)
        const blastResultsStream = fs.createReadStream('test-data/tabularBlastResults.fmt6.medium.tab')
        blastResultsStream
          .pipe(parser)
          .on('finish',() => {
            const links = parser.getData()
            // tslint:disable-next-line: no-unused-expression
            expect(links).to.not.be.undefined
            done()
          })
          .on('error',(error) => {
            expect(1 !== 1, "something is wrong")
            done()
          })
  
    })
  })
})
