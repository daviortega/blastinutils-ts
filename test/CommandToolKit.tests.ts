import chai from 'chai'
import { CommandToolKit } from '../src/CommandToolKit'
import { IBlastPParameters, IMakeBlastDBParameters, supportedProgramsType } from '../src/interfaces'

const expect = chai.expect
const logLevel = 'silent'

describe('BlastInutils', () => {
	describe('CommandsToolKit', () => {
		describe('build()', () => {
			it('should build a blastp command', () => {
        const commandTk = new CommandToolKit(logLevel)
        const fixtures: RegExp[] = [
          new RegExp(/^blastp/),
          new RegExp(/-db mydb/),
          new RegExp(/-query myquery/),
          new RegExp(/-out myoutputFile.dat/),
          new RegExp(/-num_threads 4/),
          new RegExp(/-outfmt "6 qseqid sseqid bitscore pident evalue length"/),
          new RegExp(/-evalue 1/),
        ]
        const params: IBlastPParameters = {
          db: 'mydb',
          evalue: 1,
          num_threads: 4,
          out: 'myoutputFile.dat',
          outfmt: {
            format: 6,
            parameters: [
             'qseqid',
             'sseqid',
             'bitscore',
             'pident',
             'evalue',
             'length',
            ]
          },
          query: 'myquery'
        }
        const program: supportedProgramsType = 'blastp'
				const command = commandTk.build(program, params)
        fixtures.forEach((fixRe) => {
          expect(command).match(fixRe)
        })
			})
			it('should build a makeblastdb command', () => {
        const commandTk = new CommandToolKit()
        const fixtures: RegExp[] = [
          new RegExp(/^makeblastdb/),
          new RegExp(/-in myFastaFile.fa/),
          new RegExp(/-out mydb/),
          new RegExp(/-dbtype prot/),
        ]
        const params: IMakeBlastDBParameters = {
          dbtype: 'prot',
          in: 'myFastaFile.fa',
          out: 'mydb'
        }
        const program: supportedProgramsType = 'makeblastdb'
				const command = commandTk.build(program, params)
        fixtures.forEach((fixRe) => {
          expect(command).match(fixRe)
        })
			})
		})
	})
})
